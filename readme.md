# JavaScript Template с использованием jQuery
Возможно вам будет интересен данный проект. Он не является лучшим выбором,
но для моих задач, подошел идеально. Данный скрипт поможет вам создать 
систему шаблонов с подстановкой переменных в нужные места. 
Когда мне потребовалось применять шаблоны для в JS я написал этот проект для себя. 
А теперь, использую его постоянно в своей работе. 
Поверьте, это очень удобно, тепрерь вам не приходется писать шаблоны в JS,
что добавляет больше гибкости в разработке front-end'a и back-end'a. 

## Установка
1) Подключите **jQuery** для работы с JavaScript шаблонизатором
`<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>`

2) А теперь, подключите сам шаблонизатор  
`<script src="/path/to/folder/script/templates/simple.tpl.js" type="text/javascript"></script>`

## Пример использования
В качестве шаблона вы можете использовать строку или тег шаблона `<template>` используя атрибут `data-name`.
Тег `<template></template>` добавлен в HTML5, и ваша верстка останется валидной.

*HTML код шаблона:*  
`<template data-name="test-template-js">Привет {! user.first_name !}</template>`


### tpl.include ( string name [, template data-name], json data [, json variables ] )
Для парсинга берет шаблон `<template>` и использует для него, переданные в массиве `data` переменные.

*JavaScript код*  
`tpl.include( 'test-template-js', {user : { first_name : 'Sergey', last_name : 'Mahlenko' } });`

### tpl.string( string string [, string input], json data [, json variables ] )
Вместо шаблона `<template>` использует переданную строку  
Например так: `tpl.parse('Привет {!user.first_name!}', {user : {first_name : 'Sergey'} })`