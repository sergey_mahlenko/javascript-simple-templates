/**
 * Simple Javascript template parsing
 *
 * @author Sergey Makhlenko <client@riotgroup.ru>
 * @version 0.1
 * @see https://bitbucket.org/sergey_mahlenko/javascript-simple-templates
 */
var tpl = {

    options : {
        tagTpl      : 'template',
        attrNameTpl : 'data-name',
        accessSpace : true,
        tag : {
            open: '{!',
            close: '!}',
        }
    },

    // ----------------------------------------------------------
    // Parsing user functions
    // ----------------------------------------------------------

    /**
     * Parse string
     * @param  {string} string
     * @param  {array} data
     * @return {string}
     */
    string : function( string, data ) {
        var Parser = this;

        string = Parser.parse_vars( string, data );
        return string.trim();
    },

    // ----------------------------------------------------------

    /**
     * Parse tag template
     * @param  {string} name
     * @param  {array} data
     */
    include : function( name, data ) {
        var $tpl = $( this.options.tagTpl + '['+this.options.attrNameTpl+'='+ name +']');
        var $html_string_tpl = $tpl.html().trim();

        return this.string( $html_string_tpl, data );
    },



    // ----------------------------------------------------------
    // Helpers function templates
    // ----------------------------------------------------------

    /**
     * Replace find var to string
     * @param {string} string
     * @param {string} var_name
     * @return {string}
     */
    parse_replace : function( string, var_name, replace_string ) {
        var options = this.options;
        var reg_space = '(\\s+)?';

        // Resolve space between the tags
        if ( options.accessSpace == true ) {
            options.tag.open += reg_space;
            options.tag.close = reg_space + options.tag.close;
        }

        // Replace vars string
        var exp = new RegExp(options.tag.open + var_name + options.tag.close, 'ig');
        string = string.replace( exp, replace_string );

        return string;
    },

    // ----------------------------------------------------------

    /**
     * Parse array data
     * @param  {string} string
     * @param  {array} data
     * @param  {string} key
     * @return {string}
     */
    parse_vars : function( string, data, key ) {
        var Parser = this;

        if ( typeof key == 'undefined') var key = '';

        $.each(data, function(key_name, value) {
            if ( typeof value == 'object' ) {
                string = Parser.parse_vars( string, value, key + key_name + '.' );
            } else {
                key_name = key + key_name;
                string = Parser.parse_replace( string, key_name, value );
            }
        });

        return string;
    }
}